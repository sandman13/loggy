import argparse

from loggy import server
from loggy import configcheck

def parseargs():
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", "-t", action="store_true",
                        help="Test filters")
    return parser.parse_args()

def main():
    host = "127.0.0.1"
    port = 5000

    args = parseargs()
    if args.test:
        configcheck.parse_filters()
        return

    configcheck.parse_filters()
    server.listen(host, port)

if __name__ == "__main__":
    main()
