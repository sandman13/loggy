"""Handles sockets and information associated"""
import os
import select
import sys

from loggy import utils
from loggy import filters

def writelogs(filename, msg):
    filepath = os.path.join(os.getenv("HOME"), "logs")
    if not os.path.exists(filepath):
        try:
            os.mkdir(filepath)
        except OSError as msg:
            sys.stderr.write("{0}\n".format(msg))
            sys.exit(1)

    logfile = os.path.join(filepath, filename)

    with open(logfile, 'a') as f:
        f.write(msg)

def parselog(host, port, data):
    for f in filters.filters:
        src_host = f.get("src_host")
        dst_file = f.get("dst_file")
        prog_name = f.get("prog_name")

        if src_host == host and prog_name in data:
            msg = "{0} {1}:{2} {3}".format(utils.curtimestamp(), host, port,
                                           data)
            writelogs(dst_file, msg)

def handle_data(addr, data):
    host, port =  addr
    data = data.decode("utf-8", errors="ignore")
    parselog(host, port, data)

def handle_connections(tcpsock, udpsock):
    connections = [tcpsock, udpsock]
    while connections:
        incoming, _, _= select.select(connections, [], [])

        for s in incoming:
            if s == tcpsock:
                sfd, addr = tcpsock.accept()
                sfd.setblocking(0)
                connections.append(sfd)
            elif s == udpsock:
                data, addr = s.recvfrom(1024)
                if data:
                    handle_data(addr, data)
            else:
                try:
                    data = s.recv(1024)
                except:
                    s.close()
                    if s in connections:
                        connections.remove(s)
                    continue
                
                if data:
                    handle_data(s.getpeername(), data)
                else:
                    s.close()
                    if s in connections:
                        connections.remove(s)

