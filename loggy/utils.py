"""Utilities for loggy"""
from datetime import datetime

def curtimestamp():
    now = datetime.now()
    return datetime.strftime(now, "%Y-%m-%d %H:%M:%S")
