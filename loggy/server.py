"""listen and serve"""
import socket
import sys

from loggy import handlers

def destroy(sock):
    sock.shutdown(socket.SHUT_RDWR)
    sock.close()

def listen(host, port):
    tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        tcpsock.bind((host or "127.0.0.1", port or 5000))
        tcpsock.listen(5)
        print("* Listening on {0}:{1} [TCP]".format(*tcpsock.getsockname()))

        udpsock.bind((host or "127.0.0.1", port or 5000))
        print("* Listening on {0}:{1} [UDP]".format(*udpsock.getsockname()))

        handlers.handle_connections(tcpsock, udpsock)
    except socket.error as msg:
        print(msg)
        destroy(tcpsock)
        destroy(udpsock)
    except KeyboardInterrupt:
        print("\rInterrupt detected, shutting down...")
