"""List of filters to be applied to incoming logs"""
filters = [
            {
                "src_host": "127.0.0.1", "dst_file": "generic.log",
                "prog_name": "logprog", "proto": "tcp"
            },
            {
                "src_host": "127.0.0.1", "dst_file": "debian.log",
                "prog_name": "debian", "proto": "tcp"
            },

          ]
