import socket
import time

def sendtcp(host, port, msg):
    msg = [ "TCP: " + m for m in msg ]
    for m in msg:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        s.send(m)
        s.close()

def sendudp(host, port, msg):
    msg = [ "UDP: " + m for m in msg ]
    for m in msg:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.sendto(m, (host, port))
        s.close()

def main():
    host = "127.0.0.1"
    port = 5000
    msg = ["Hello there, how are you doing?\r\n",
            "Is everthing alright?\r\n",
            "debian logs\r\n"]
    sendtcp(host, port, msg)
    sendudp(host, port, msg)

if __name__ == "__main__":
	main()	
