import socket
from loggy import filters

def parse_filters():
    for i, f in enumerate(filters.filters):
        src_host = f.get("src_host")
        prog_name = f.get("prog_name")

        if not src_host and not prog_name:
            msg = "On item: {0}, both 'src_host' and 'prog_name' are missing\n"
            sys.stderr.write(msg.format(i))
            sys.exit(1)

        try:
            socket.inet_aton(src_host)
        except socket.error:
            sys.exit(1)
    print("Configuration check: Passed")
